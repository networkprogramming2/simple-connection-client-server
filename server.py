import socket

s = socket.socket()
print("Socket successfully created")

# maximum port is 65535
port = 25697

s.bind(('127.0.0.1', port))
print("socket binded to %s" % port)

s.listen(5)
print("socket is listening")


def sendNumber(client, number):
    updated_number = number + 1
    client.send(str(updated_number).encode())
    data = client.recv(1024).decode()
    return int(data)


number = 0

c, addr = s.accept()
print('Got connection from', addr)

try:
    while number < 100:
        number = sendNumber(c, number)
        print(f'Received: {number}')

except ConnectionResetError:
    print('Client disconnected unexpectedly')

finally:
    c.close()

s.close()
