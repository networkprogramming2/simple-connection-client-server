import socket

s = socket.socket()

# maximum port is 65535
port = 25697

s.connect(('127.0.0.1', port))


def sendNumber(server, number):
    data = server.recv(1024).decode()
    if data == '':
        print('Server disconnected unexpectedly')
        return number

    received_number = int(data)
    print(f'Received: {received_number}')

    updated_number = received_number + 1
    server.send(str(updated_number).encode())
    return updated_number

number = 0
try:

    while number < 100:
        number = sendNumber(s, number)

except ConnectionAbortedError:
    print('Server disconnected unexpectedly')

finally:
    s.close()
